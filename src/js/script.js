var focusApp = angular.module ('focusApp', ['ngRoute'])

focusApp.config (function ($routeProvider) {
    $routeProvider
        .when ('/', {
            templateUrl: 'pages/login.html',
            controller: 'loginController',
        })
        .when ('/user', {
            templateUrl: 'pages/user.html',
            controller: 'loginController',
        })
        .when ('/apps', {
            templateUrl: 'pages/apps.html',
    });
});

focusApp.controller ('indexController', function ($scope, $location) {
    $scope.isUserPage = function () {
        return $location.path () == '/user';
    }
});

focusApp.controller ('loginController', function ($scope, $location) {
    var shakeArea = document.getElementById ('shakeArea');
    $scope.wrongPass = '';
    $scope.passwordKeyUp = function (event) {
        if (event.keyCode == 13) {
            $scope.wrongPass = 'shake';
        }
        else {
            $scope.wrongPass = '';
        }
    };
});
